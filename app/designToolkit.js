/*jslint browser: true*/
/*jslint plusplus: true */
/*global angular,FileReader,localStorage, Object, console*/

var designToolkit = angular.module("com.designToolkit", ["ngMaterial", "com.designToolkit.controller", "com.designToolkit.factory", "ui.router", "com.designToolkit.directive", "com.designToolkit.iosTabletController"])
    .config(function ($mdThemingProvider, $stateProvider, $urlRouterProvider) {
        "use strict";
        $mdThemingProvider.theme('default')
            .primaryPalette('orange')
            .accentPalette('brown');

        $urlRouterProvider.otherwise("/ios");
        //
        // Now set up the states
        $stateProvider
            .state('iOS', {
                url: "/ios",
                views: {
                    main: {
                        templateUrl: "mainView",
                        controller: "mainController"
                    },
                    sideBar: {
                        templateUrl: "sideBar",
                        controller: function ($scope, loadJSON) {
                            loadJSON.load("mobileInputFields.json", function (fields) {
                                console.log("List of fields", fields);
                                $scope.fields = fields.data;
                            });

                        }
                    }
                }
            })
            .state('Android', {
                url: "/Android",
                views: {
                    main: {
                        templateUrl: "mainView",
                        controller: "mainController"
                    },
                    sideBar: {
                        templateUrl: "sideBar",
                        controller: function ($scope, loadJSON) {

                        }
                    }
                }
            })
            .state('WebBrowser', {
                url: "/WebBrowser",
                views: {
                    main: {
                        templateUrl: "mainView",
                        controller: "mainController"
                    },
                    sideBar: {
                        templateUrl: "sideBar",
                        controller: function ($scope, loadJSON) {

                        }
                    }
                }
            })
            .state('iOSTablet', {
                url: "/iOSTablet",
                views: {
                    main: {
                        templateUrl: "mainView",
                        controller: "iosTabletController"
                    },
                    sideBar: {
                        templateUrl: "sideBar",
                        controller: function ($scope, loadJSON) {
                            loadJSON.load("tabletInputFields.json", function (fields) {
                                console.log("List of fields", fields);
                                $scope.fields = fields.data;
                            });
                        }
                    }
                }

            })
            .state('AndroidTablet', {
                url: "/AndroidTablet",
                views: {
                    main: {
                        templateUrl: "mainView",
                        controller: "mainController"
                    },
                    sideBar: {
                        templateUrl: "sideBar",
                        controller: function ($scope, loadJSON) {

                        }
                    }
                }
            });
    });


/**
 * @ngdoc module
 * @name com.designToolkit.factory
 *
 * @description
 * factory module
 */
var designToolkitFactory = angular.module("com.designToolkit.factory", []);

/**
 * @ngdoc module
 * @name selectedIndex
 *
 * @description
 * selectedIndex factory module
 */
designToolkitFactory.factory("selectedIndex", function () {
    "use strict";

    var index;

    return {
        /**
         *Return the tab index
         *@return {number} Tab Index
         */
        get: function () {
            return index;
        },
        set: function (n) {
            index = n;
        }

    };
});

/**
 * @ngdoc module
 * @name fileImport
 *
 * @description
 * fileImport factory module
 */
designToolkitFactory.factory("fileImport", function ($q) {

    "use strict";

    return {
        /**
         *Read file from file system using HTML 5
         *@param {object} File to load
         *@return {object} Promise
         */
        read: function (file) {
            var deferred, reader;

            deferred = $q.defer();

            reader = new FileReader();

            reader.onload = function (theFile) {
                deferred.resolve(theFile.target.result);
            };
            reader.readAsDataURL(file);

            return deferred.promise;
        },
        /**
         *Load data from file
         *@param {string} Data to load
         *@return
         */
        load: function (data) {

        }
    };
});

/**
 * @ngdoc module
 * @name saveColorData
 *
 * @description
 * saveColorData factory module
 */
designToolkitFactory.factory("saveColorData", function () {
    "use strict";

    return {
        set: function (o) {
            var key = o.key || "colorData";
            localStorage.setItem(key, JSON.stringify(o.data));
        },
        get: function (k) {
            var key = k || "colorData";
            return JSON.parse(localStorage.getItem(key));
        },
        remove: function (k) {
            var key = k || "colorData";
            localStorage.removeItem(key);
        }

    };
});

/**
 * @ngdoc module
 * @name imagePlaceholder
 *
 * @description
 * imagePlaceholder factory module
 */
designToolkitFactory.factory("imagePlaceholder", function () {
    "use strict";

    function createImagePlaceholdersArray(v) {
        return v.divClass.split(" ")[0];
    }

    return {
        init: function (obj) {
            this.scope = obj.scope;
            this.tabIndex = obj.tabIndex;
            this.imageName = obj.image;
        },
        setImagePlaceholderIndex: function () {
            //grab the first values from the loadImageDivs.imagePlaceholders from the current visual tab
            var imagePlaceholders = this.scope.tabs[this.tabIndex].imagePlaceholders.map(createImagePlaceholdersArray);
            //set the index of image to change color
            this.imagePlaceholderIndex = imagePlaceholders.indexOf(this.imageName);
        },
        getImagePlaceholderIndex: function () {
            return this.imagePlaceholderIndex;
        },
        setBackGround: function (color) {
            var value, divImage;

            this.setImagePlaceholderIndex();

            if (this.getImagePlaceholderIndex() !== -1) {
                divImage = this.scope.tabs[this.tabIndex].imagePlaceholders[this.getImagePlaceholderIndex()];

                if (typeof color.value !== "string") {
                    value = "linear-gradient(to bottom, #" + color.value.top + " 0%, #" + color.value.bottom + " 100%)";
                } else {
                    value = '#' + color.value;
                }

                if (color.border === true) {
                    divImage.style = {
                        'border-color': value
                    };
                } else {
                    //set background color of div image
                    divImage.style = {
                        'background': value
                    };
                }

            }
        }
    };
});

/**
 * @ngdoc module
 * @name loadImageDivs
 *
 * @description
 * loadImageDivs factory module
 */
designToolkitFactory.factory("gradientData", function () {
    "use strict";
    var gradient = {};
    return {
        set: function (o) {
            gradient[o.data] = o.value;
        },
        get: function (o) {
            if (gradient.hasOwnProperty(o.data)) {
                return gradient[o.data].length > 0 ? gradient[o.data] : null;
            }
            return null;
        }
    };
});

/**
 * @ngdoc module
 * @name loadJSON
 *
 * @description
 * loadJSON factory module
 */
designToolkitFactory.factory("loadJSON", ["$http", function ($http, $q, $log) {
    "use strict";
    return {
        load: function (file, cb) {
            // Simple GET request example :
            $http.get('model/' + file)
                .success(function (data, status, headers, config) {
                    cb(data);
                })
                .error(function (data, status, headers, config) {
                    $log.error(data);
                });
        }
    };
}]);

/**
 * @ngdoc module
 * @name com.designToolkit.directive
 *
 * @description
 * directive module
 */
var designToolkitDirective = angular.module("com.designToolkit.directive", []);

designToolkitDirective.directive("mobileBackground", function ($log, $rootScope) {
    "use strict";
    return {
        templateUrl: "mobileBackDrop",
        replace: true,
        scope: {
            "mbSrc": "@",
            "mbText": "@",
            "mbClass": "@"
        },
        link: function (scope, element, attr) {
            var evt;
            switch (scope.mbText) {
            case "splash":
                evt = "onSplash";
                attr.$set("style", "z-index:10");
                break;
            case "home":
                evt = "onHome";
                element.addClass("ios-icon");
                break;
            default:
                evt = "onBackground";

            }

            $rootScope.$on(evt, function (event, data) {
                $log.warn("Image data", data);
                attr.$set("src", data);
            });
        }
    };
});

designToolkitDirective.directive("mobileImage", function ($log, $rootScope, gradientData, saveColorData) {
    "use strict";
    return {
        templateUrl: "mobileDiv",
        replace: true,
        transclude: true,
        scope: {
            "mbStyle": "@",
            "mbClass": "@"
        },
        link: function (scope, element, attr) {
            //infoPanelTop & infoPanelBottom
            var evt, colorElements, style, colorData, top, bottom;

            //reads the images file "divClass": "infoPanel", property to create event listener
            evt = attr.mbClass.split(" ")[0];
            //Pull data from localstorage
            colorData = saveColorData.get();
            //checks to sees if property exist i.e. infoPanelTop
            //yes updates the gradient section
            //No set the standard background
            if (colorData.hasOwnProperty(evt + "Top")) {
                top = colorData[evt + "Top"];
                bottom = colorData[evt + "Bottom"];
                style = "background:linear-gradient(to bottom, #" + top + " 0%, #" + bottom + " 100%)";

            } else {
                style = "background:#" + colorData[evt];
            }

            //add style attribute to div 
            attr.$set("style", style);

            $log.warn("evt", evt);
            //            debugger;

            //create event listener based on mbClass attribute
            $rootScope.$on(evt, function (events, value, model) {
                var style, top, bottom;


                if (model.search('Top') > 0 || model.search('Bottom') > 0) {
                    gradientData.set({
                        data: model,
                        value: value
                    });

                    top = gradientData.get({
                        data: events.name + "Top"
                    });

                    bottom = gradientData.get({
                        data: events.name + "Bottom"
                    });
                }

                if (top && bottom) {

                    style = "background:linear-gradient(to bottom, #" + top + " 0%, #" + bottom + " 100%)";

                } else {
                    style = "background:#" + value;
                }


                attr.$set("style", style);
            });
        }
    };
});