/*jslint browser: true*/
/*jslint plusplus: true */
/*global angular,FileReader,localStorage, Object*/

(function () {
    "use strict";
    /**
     * @ngdoc module
     * @name com.designToolkit.controller
     *
     * @description
     * controller module
     */
    var designToolkitController = angular.module("com.designToolkit.iosTabletController", ["flow"]);

    /**
     * @ngdoc module
     * @name mainController
     *
     * @description
     * mainController controller module
     */
    designToolkitController.controller("iosTabletController", function ($rootScope, $scope, $timeout, loadJSON, fileImport, $log, saveColorData, $mdSidenav, selectedIndex, $state) {

        function init() {
            var x, colorElements;

            loadJSON.load("iosTabletImages.json", function (images) {
                $log.warn("List of images", images);
                $scope.tabs = images.data;
            });
        }

        init();

    });

}());