/*jslint browser: true*/
/*jslint plusplus: true */
/*global angular,FileReader,localStorage, Object*/

(function () {
    "use strict";
    /**
     * @ngdoc module
     * @name com.designToolkit.controller
     *
     * @description
     * controller module
     */
    var designToolkitController = angular.module("com.designToolkit.controller", ["flow"]);

    /**
     * @ngdoc module
     * @name mainController
     *
     * @description
     * mainController controller module
     */
    designToolkitController.controller("mainController", function ($rootScope, $scope, $timeout, loadJSON, fileImport, $log, saveColorData, $mdSidenav, selectedIndex, $state) {
        var selectedIndexTab;
        //wait to set the selected device dropdown box
        $timeout(function () {
            $scope.device = $state.current.name;
        }, 2000);

        //change the page based on the selected device dropdown box
        $scope.$watch('device', function () {
            if ($scope.device !== undefined) {
                $state.go($scope.device);
            }

        });


        function init() {
            var x, colorElements;

            loadJSON.load("iosImages.json", function (images) {
                $log.warn("List of images", images);
                $scope.tabs = images.data;

                $scope.colors = saveColorData.get() || {};

                //loads the saved background image for tabs start from the 3rd tab on
                for (x = 2; x < $scope.tabs.length; x++) {
                    $scope.tabs[x].background = saveColorData.get('background');
                }

                //loads the saved icon image on 2nd tab
                $scope.tabs[1].background = saveColorData.get('iosIcon');
                $scope.tabs[1].ngclass = "ios-icon";

                //loads the saved splash screen image on 1st tab
                if (saveColorData.get('splashScreen')) {
                    $scope.tabs[0].image = saveColorData.get('splashScreen');
                }

                //loads the saved color for each div
                if (Object.keys($scope.colors).length > 0) {
                    colorElements = Object.keys(saveColorData.get());
                    colorElements.forEach(function (v) {
                        $scope.updateColor(v);
                    });
                }
            });
        }

        //Toolbar Navigation starts
        $scope.links = [{
            name: "iOS",
            link: "iOS"
        }, {
            name: "Android",
            link: "Android"
        }, {
            name: "Web Browser",
            link: "WebBrowser"
        }, {
            name: "iOS Tablet",
            link: "iOSTablet"
        }, {
            name: "Android Tablet",
            link: "AndroidTablet"
        }];

        $scope.openMenu = function () {
            $mdSidenav('sideBar').toggle();
        };
        //Toolbar Navigation ends

        $scope.importHandler = function ($flow) {
            fileImport.read($flow.files[0].file).then(function (data) {
                var result, x;
                result = fileImport.load(data);
                if (selectedIndex.get() === 0) {
                    $rootScope.$broadcast("onSplash", data);
                    saveColorData.set({
                        key: 'splashScreen',
                        data: data
                    });
                } else if (selectedIndex.get() === 1) {
                    $rootScope.$broadcast("onHome", data);
                    saveColorData.set({
                        key: 'iosIcon',
                        data: data
                    });
                } else {
                    //                    for (x = 2; x < $scope.tabs.length; x++) {
                    $rootScope.$broadcast("onBackground", data);
                    saveColorData.set({
                        key: 'background',
                        data: data
                    });
                    //                    }
                }

                $flow.cancel();
                $log.warn("selected tab", selectedIndex.get());
                $log.warn('File information', data);
            });
        };

        $scope.$watch('selectedIndexTab', function (n, o) {
            if (n !== o) {
                selectedIndex.set(n);
                $log.warn("selected index tab", n);
            }
        });


        $scope.updateColor = function (imageName) {
            var x, evt;
            //evt fired is based on the "model": "infoPanelTop" minus Top or Bottom
            evt = imageName.search('Top') > 0 ? imageName.substr(0, imageName.search('Top')) : imageName;
            evt = imageName.search('Bottom') > 0 ? imageName.substr(0, imageName.search('Bottom')) : evt;
            //pull color directly from the dynamic $scope.color model
            $rootScope.$broadcast(evt, $scope.colors[imageName], imageName);

            saveColorData.set({
                data: $scope.colors
            });
        };

        init();


    });

}());